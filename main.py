import sys, pygame

def add_tiles_on_scene(screen):
    for i in range(10):
        for j in range(10):
            image = pygame.image.load('resources/game_sprites/grass.bmp').convert_alpha(screen)
            screen.blit(image, (32*i, 32*j))


def start():
    pygame.init()
    size = 320, 320
    black = 0, 0, 0

    screen = pygame.display.set_mode(size)

    screen.fill(black)

    for i in range(10):
        for j in range(10):
            image = pygame.image.load('resources/game_sprites/grass.bmp').convert_alpha(screen)
            screen.blit(image, (32 * j, 32 * i))

            pygame.display.flip()

    while 1:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()






if __name__ == '__main__':
    start()
